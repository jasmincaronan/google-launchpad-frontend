import campaignPerformanceTemplate from './campaignPerformance.template.html'
import {CampaignPerformanceController} from './CampaignPerformanceController'
export default {
	
	template: campaignPerformanceTemplate,
	controller: CampaignPerformanceController 	
}