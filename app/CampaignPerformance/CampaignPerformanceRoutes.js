export default function CampaignPerformanceRoutes($stateProvider) {
  $stateProvider
    .state({
      name: 'campaign_performance',
      url: '/campaign_performance',
      template: '<campaign_performance/>'
    })
}

CampaignPerformanceRoutes.$inject = [ '$stateProvider' ]