import angular from 'angular'
import 'angular-ui-router'
import CampaignPerformanceRoutes from './CampaignPerformanceRoutes'
import CampaignPerformanceComponent from './CampaignPerformanceComponent'

angular.module('CampaignPerformanceModule', ['ui.router'])
  .component('campaignPerformance', CampaignPerformanceComponent)
  .config(CampaignPerformanceRoutes)
