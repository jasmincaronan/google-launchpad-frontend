export default function DashboardRoutes($stateProvider) {
  $stateProvider
    .state({
      name: 'dashboard',
      url: '/dashboard',
      template: '<dashboard/>'
    })
}

DashboardRoutes.$inject = [ '$stateProvider' ]