export class DashboardController {
  constructor() {
    this.load();
  }

  load() {
    console.log("loaded dashboard controller")
    this.title = "TEST"
    this.alerts = [{
      type: 'success',
      msg: 'Thanks for visiting! Feel free to create pull requests to improve the dashboard!'
    }, {
      type: 'danger',
      msg: 'Found a bug? Create an issue with as many details as you can.'
    }];
  }

  toggleSidebar() {
    console.log("toggled sidebar")
    this.toggle = !this.toggle;
  }

}

DashboardController.$inject = []
