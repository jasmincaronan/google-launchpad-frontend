import angular from 'angular'
import 'angular-ui-bootstrap'
import './Login/LoginModule'
import './CampaignPerformance/CampaignPerformanceModule'
import './Dashboard/DashboardModule'
import './../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './../bower_components/rdash-ui/dist/css/rdash.min.css'
import './../node_modules/font-awesome/css/font-awesome.min.css'
import './css/custom_main.css'


angular.module('App', [
  'ui.bootstrap',
  'LoginModule',
  'CampaignPerformanceModule',
  'DashboardModule'
  ])