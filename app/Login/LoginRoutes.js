export default function LoginRoutes($stateProvider) {
  $stateProvider
    .state({
      name: 'login',
      url: '',
      template: '<login/>'
    })
}

LoginRoutes.$inject = [ '$stateProvider' ]