import loginTemplate from './login.template.html'
import {LoginController} from './LoginController'
export default {

  template: loginTemplate,
  controller: LoginController
}
