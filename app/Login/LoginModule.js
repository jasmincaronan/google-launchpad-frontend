import angular from 'angular'
import 'angular-ui-router'
import LoginRoutes from './LoginRoutes'
import LoginComponent from './LoginComponent'

angular.module('LoginModule', ['ui.router'])
  .component('login', LoginComponent)
  .config(LoginRoutes)
